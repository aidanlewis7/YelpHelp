# main.py

import cherrypy

from _yelp_database import _yelp_database
from ambience import ambience
from cuisine import cuisine
from quiz import quiz
from review import review
from visited_restaurant import visitedRestaurant

from options_controller import OptionsController
from reset_controller import ResetController
from StateController import StateController
from AmbienceController import AmbienceController
from CuisineController import CuisineController
from QuizController import QuizController
from visited_restaurant_controller import VisitedRestaurantController
from ReviewController import ReviewController


import os
import json, re

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "*"

def start_service():
    ydb = _yelp_database()
    c = cuisine(ydb)    # ARE THESE THE CORRECT DECLARATIONS?
    a = ambience(ydb)
    r = review(ydb)
    vr = visitedRestaurant(ydb)
    q = quiz(c, a)

    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    # OptionsController ------------------------------------------------------
    optionsController = OptionsController()
    options_dispatcher(optionsController, dispatcher)

    # ResetController ---------------------------------------------------------
    resetController = ResetController(ydb)
    reset_dispatcher(resetController, dispatcher)

    # StatesController ---------------------------------------------------------
    stateController = StateController(ydb)
    state_dispatcher(stateController, dispatcher)

    # AmbienceController --------------------------------------------------------
    ambienceController = AmbienceController(a)
    ambience_dispatcher(ambienceController, dispatcher)

    # CuisineController ---------------------------------------------------------
    cuisineController = CuisineController(c)
    cuisine_dispatcher(cuisineController, dispatcher)

    # QuizController ---------------------------------------------------------
    quizController = QuizController(q)
    quiz_dispatcher(quizController, dispatcher)

    # VisitedRestaurantController ---------------------------------------------------------
    visitedRestaurantController = VisitedRestaurantController(vr)
    visited_restaurant_dispatcher(visitedRestaurantController, dispatcher)

    # ReviewController -----------------------------------------------
    reviewController = ReviewController(r)
    review_controller_dispatcher(reviewController, dispatcher)

    conf = { 'global' : {'server.socket_host' : 'student04.cse.nd.edu',
                            'server.socket_port' : 51039 },
            '/' : {'request.dispatch' : dispatcher, 'tools.CORS.on' : True }
    }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)


def reset_dispatcher(resetController, dispatcher):
    dispatcher.connect('reset', '/reset/',
    controller=resetController,
    action = 'RESET', conditions=dict(method=['PUT']))


def state_dispatcher(stateController, dispatcher):
    dispatcher.connect('get_state_options', '/state_options/',
    controller=stateController,
    action = 'GET_STATE_OPTIONS', conditions=dict(method=['GET']))


def ambience_dispatcher(ambienceController, dispatcher):

    dispatcher.connect('get_ambience_options', '/ambience_options/',
    controller=ambienceController,
    action = 'GET_AMBIENCE_OPTIONS', conditions=dict(method=['GET']))

    dispatcher.connect('get_ambience_all_locations', '/ambience/all_locations/',
    controller=ambienceController,
    action = 'GET_AMBIENCE_ALL_LOCATIONS', conditions=dict(method=['GET']))

    dispatcher.connect('get_ambience', '/ambience/',
    controller=ambienceController,
    action = 'GET_AMBIENCE', conditions=dict(method=['GET']))

    dispatcher.connect('get_ambience_percentage', '/ambience/percentage/',
    controller=ambienceController,
    action = 'GET_AMBIENCE_PERCENTAGE', conditions=dict(method=['GET']))

    dispatcher.connect('get_ambience_all_percentages', '/ambience/all_percentages/',
    controller=ambienceController,
    action = 'GET_AMBIENCE_ALL_PERCENTAGES', conditions=dict(method=['GET']))

    dispatcher.connect('get_ambience_ratings', '/ambience/ratings/',
    controller=ambienceController,
    action = 'GET_AMBIENCE_RATINGS', conditions=dict(method=['GET']))


def cuisine_dispatcher(cuisineController, dispatcher):

    dispatcher.connect('get_cuisine_options', '/cuisine_options/',
    controller=cuisineController,
    action = 'GET_CUISINE_OPTIONS', conditions=dict(method=['GET']))

    dispatcher.connect('get_cuisine_all_locations', '/cuisine/all_locations/',
    controller=cuisineController,
    action = 'GET_CUISINE_ALL_LOCATIONS', conditions=dict(method=['GET']))

    dispatcher.connect('get_cuisine', '/cuisine/',
    controller=cuisineController,
    action = 'GET_CUISINE', conditions=dict(method=['GET']))

    dispatcher.connect('get_cuisine_percentage', '/cuisine/percentage/',
    controller=cuisineController,
    action = 'GET_CUISINE_PERCENTAGE', conditions=dict(method=['GET']))

    dispatcher.connect('get_cuisine_all_percentages', '/cuisine/all_percentages/',
    controller=cuisineController,
    action = 'GET_CUISINE_ALL_PERCENTAGES', conditions=dict(method=['GET']))

    dispatcher.connect('get_cuisine_ratings', '/cuisine/ratings/',
    controller=cuisineController,
    action = 'GET_CUISINE_RATINGS', conditions=dict(method=['GET']))


def quiz_dispatcher(quizController, dispatcher):

    dispatcher.connect('get_quiz_result', '/quiz/',
    controller=quizController,
    action = 'GET_QUIZ_RESULT', conditions=dict(method=['GET']))


def visited_restaurant_dispatcher(visitedRestaurantController, dispatcher):
    dispatcher.connect('visited_restaurant_get', '/favorites/',
    controller=visitedRestaurantController,
    action = 'GET_FAVORITES', conditions=dict(method=['GET']))

    dispatcher.connect('visited_restaurant_post', '/favorites/',
    controller=visitedRestaurantController,
    action = 'POST_TO_FAVORITES', conditions=dict(method=['POST']))

    dispatcher.connect('visited_restaurant_delete', '/favorites/',
    controller=visitedRestaurantController,
    action = "DELETE_FAVORITE", conditions=dict(method=['PUT']))


def review_controller_dispatcher(reviewController, dispatcher):
	dispatcher.connect('get_reviews', '/reviews/',
	controller=reviewController,
	action = 'GET_REVIEWS', conditions=dict(method=['GET']))

	dispatcher.connect('post_review', '/reviews/',
	controller=reviewController,
	action = 'POST_TO_REVIEWS', conditions=dict(method=['POST']))

def options_dispatcher(optionsController, dispatcher):

    # ------------------------- RESET ----------------------------------------

    dispatcher.connect('options_reset', '/reset/',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    # ------------------------- STATE ----------------------------------------

    dispatcher.connect('options_get_state_options', '/state_options/',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    # ------------------------- AMBIENCE --------------------------------------

    dispatcher.connect('options_ambience_get_options', '/ambience_options/',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    dispatcher.connect('options_ambience_get_all_locations', '/ambience/all_locations/',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    dispatcher.connect('options_ambience_get', '/ambience/',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    dispatcher.connect('options_ambience_get_percentage', '/ambience/percentage',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    dispatcher.connect('options_ambience_get_all_percentages', '/ambience/all_percentages',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    dispatcher.connect('options_get_ambience_ratings', '/ambience/ratings/',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    # ------------------------- CUISINE ---------------------------------------

    dispatcher.connect('options_cuisine_get_options', '/cuisine_options/',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    dispatcher.connect('options_cuisine_get_all_locations', '/cuisine/all_locations/',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    dispatcher.connect('options_cuisine_get', '/cuisine/',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    dispatcher.connect('options_cuisine_get_percentage', '/cuisine/percentage',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    dispatcher.connect('options_cuisine_get_all_percentages', '/cuisine/all_percentages',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    dispatcher.connect('options_get_cuisine_ratings', '/cuisine/ratings/',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    # ----------------------------- QUIZ -------------------------------

    dispatcher.connect('options_get_quiz_result', '/quiz/',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    # ---------------------- VISITED RESTAURANT -------------------------------

    dispatcher.connect('options_visited_restaurant_get', '/favorites/',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    # dispatcher.connect('options_visited_restaurant_delete', '/favorites/',
    # controller=optionsController,
    # action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    # dispatcher.connect('options_visited_restaurant_delete', '/favorites/',
    # controller=optionsController,
    # action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

	# ---------------------- REVIEW -------------------------------------------
    dispatcher.connect('options_review_get', '/reviews/',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))

    dispatcher.connect('options_review_post', '/reviews/',
    controller=optionsController,
    action = 'OPTIONS', conditions=dict(method=['OPTIONS']))


if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()
