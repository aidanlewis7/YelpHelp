import re, json, requests
import os
import cherrypy

from ambience import ambience

class AmbienceController:
    def __init__(self, a):
        self.a = a

    # returns all ambience options
    def GET_AMBIENCE_OPTIONS(self):
        output = {"result": "success"}

        try:
            output["ambiences"] = self.a.get_options()
        except:
            output["ambiences"].append({"result": "error"})
            output["result"] = "error"

        return json.dumps(output)


    # returns all restaurants with a given ambience (everywhere)
    def GET_AMBIENCE_ALL_LOCATIONS(self, ambience_type):
        output = {"restaurants": [], "result": "success"}

        try:
            for r in self.a.get_all_locations(ambience_type):
                output["restaurants"].append(r)
        except:
            output["restaurants"].append({"result": "error"})
            output["result"] = "error"

        return json.dumps(output)


    # returns all restaurants with a given ambience in a specific region
    def GET_AMBIENCE(self, ambience_type, location_type, location):
        output = {"restaurants": [], "result": "success"}

        try:
            for r in self.a.get(location_type, location, ambience_type):
                output["restaurants"].append(r["name"])
        except:
            output["restaurants"].append({"result": "error"})
            output["result"] = "error"

        return json.dumps(output)

    # returns percentage of restaurants in a region with a specific ambience
    def GET_AMBIENCE_PERCENTAGE(self, ambience_type, location_type, location):
        output = {"percentage": 0, "result": "success"}

        try:
            output["percentage"] = self.a.get_percentage(location_type, location, ambience_type)
        except:
            output["result"] = "error"

        return json.dumps(output)

    # returns percentage of restaurants in a region with a specific ambience for all regions
    def GET_AMBIENCE_ALL_PERCENTAGES(self, location_type, ambience_type):
        output = {"locations": {}, "result": "success"}

        try:
            for k, v in self.a.get_all_percentages(location_type, ambience_type).items():
                output["locations"][k] = v
        except:
            output["result"] = "error"

        return json.dumps(output)

    # returns all restaurants above a certain rating with specific ambience in a region
    def GET_AMBIENCE_RATINGS(self, location_type, location, ambience_type, rating):
        output = {"restaurants": [], "result": "success"}

        try:
            restaurants, trash = self.a.get_ratings(location_type, location, ambience_type, float(rating))

            for r in restaurants:
                output["restaurants"].append(r["name"])
        except:
            output["restaurants"].append({"result": "error"})
            output["result"] = "error"

        return json.dumps(output)
