from _yelp_database import _yelp_database
from collections import defaultdict


class cuisine:

    # inherits from _yelp_database class
    def __init__(self, database):
        self.database = database
        self.data = database.data
        self.locations = database.locations

    def get_options(self):

        cuisines = defaultdict(int)

        for restaurant in self.data.values():
            if 'categories' in restaurant:
                for cuisine in restaurant['categories']:
                    cuisines[cuisine] += 1

        cuisines = self.database.sort_dict_by_value(cuisines)[2:35]
        to_delete = ["Nightlife", "Fast Food", "Burgers", "Chicken Wings", "Event Planning & Services", "Sushi Bars", "Asian Fusion", "Caterers", "Specialty Food", "Desserts",
                    "Breakfast & Brunch", "Coffee & Tea", "Sports Bars", "Sandwiches", "Pizza", "Bars", "Diners", "American (New)", "Pubs", "Delis", "Salad", "Bakeries", "Cafes"]

        shortened_cuisines = sorted(list(set(cuisines) - set(to_delete)))

        return shortened_cuisines


    def get_all_locations(self, cuisine):

        restaurants = []

        # loop through all restaurants
        for restaurant in self.data.values():

            # if restuarant in specific region and has requested cuisine, add to list
            if 'categories' in restaurant and cuisine in restaurant['categories']:
                restaurants.append(restaurant)

        return restaurants


    def get(self, location_type, location, cuisine):

        restaurants = []

        # loop through all restaurants
        for restaurant in self.data.values():

            # if restuarant in specific region and has requested cuisine, add to list
            if restaurant[location_type] == location and 'categories' in restaurant \
            and cuisine in restaurant['categories']:
                restaurants.append(restaurant)

        return restaurants

    def get_percentage(self, location_type, location, cuisine):
        # get percentage of restaurants in this region with this cuisine
        restaurants = self.get(location_type, location, cuisine)
        return 100 * len(restaurants) / float(self.locations[location_type][location])


    def get_all_percentages(self, location_type, cuisine):

        # get percentage for every region of specified type
        restaurants = dict()

        for location in self.locations[location_type].keys():
            restaurants[location] = self.get_percentage(location_type, location, cuisine)

        #return self.database.sort_dict_by_value(restaurants)
        return restaurants

    def get_ratings(self, location_type, location, cuisine, rating):
        rated_restaurants = []
        ratings = []
        restaurants = self.get(location_type, location, cuisine)

        # for every restaurant with this cuisine in this region
        for restaurant in restaurants:

            # only append restaurant if above specified rating
            if restaurant['stars'] >= rating:
                rated_restaurants.append(restaurant)
                ratings.append(restaurant['stars'])

        return rated_restaurants, ratings
