var initial_mid = 456
var port = 51039

Label.prototype = new Item()
Button.prototype = new Item()
Div.prototype = new Item()
Dropdown.prototype = new Item()

// Header Div
var header_div = new Div()
header_div.createDiv("header_div")

// Topic Div
var topic_div = new Div()
topic_div.createDiv("topic_div")

// Topic Label
var topic_label = new Label()
topic_label.createLabel("Topic:", "topic_label")
topic_div.addItemToDiv(topic_label.item)

// Topic Dropdown
topic_dict = {"cuisine": "Cuisine", "ambience": "Ambience"}
var topic_dropdown = new Dropdown()
topic_dropdown.createDropdown(topic_dict, "topic_dict")
topic_dropdown.item.setAttribute("onchange", "dropdownChanged(topic_dropdown, topic_type_dropdown, port)");

topic_div.addItemToDiv(topic_dropdown.item)


// Topic Type Div
var topic_type_div = new Div()
topic_type_div.createDiv("topic_type_div")

// Topic Type Dropdown
var topic_type_dropdown = new Dropdown()

getOptions(topic_type_div, topic_type_dropdown, "cuisine", port)

header_div.addItemToDiv(topic_div.item)
header_div.addItemToDiv(topic_type_div.item)

// ------------------------------- Location ------------------------------------

// Location Div
var location_div = new Div()
location_div.createDiv("location_div")

// Location Label
var location_label = new Label()
location_label.createLabel("By:", "location_label")

location_div.addItemToDiv(location_label.item)

// Location Dropdown
location_dict = {"state": "State", "city": "City"}
var location_dropdown = new Dropdown()
location_dropdown.createDropdown(location_dict, "location_dict")


location_div.addItemToDiv(location_dropdown.item)
header_div.addItemToDiv(location_div.item)

// Submit Button
var submit_div = new Div()
submit_div.createDiv("submit_div")

var submit_button = new Button()
submit_button.createButton("See the map!", "submit_button")
submit_button.addClickEventHandler(submit, [topic_dropdown, topic_type_dropdown, location_dropdown, port])
submit_div.addItemToDiv(submit_button.item)
header_div.addItemToDiv(submit_div.item)

header_div.addToDocument()
