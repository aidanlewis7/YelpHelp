// delete previous graph div and make a new one
function prepareGraph()
{
    try {
        var element = document.getElementById("graph_div")
        element.parentNode.removeChild(element)
    }
    catch (e) {
        var temp = {}
    }

    div = new Div()

    div.createDiv("graph_div")
    div.item.setAttribute("style", "width:600px;height:250px;")
    div.addToDocument()

    return document.getElementById('graph_div')
}


function getDictKeys(dict)
{
    var keys = []

    for (var key in dict)
    {
        keys.push(key)
    }

    return keys
}

function getDictValues(dict)
{
    var values = []

    for (var key in dict)
    {
        values.push(dict[key])
    }

    return values
}

function getStateNames(percentages)
{
    all_states = {
    "AL": "Alabama",
    "AK": "Alaska",
    "AZ": "Arizona",
    "AR": "Arkansas",
    "CA": "California",
    "CO": "Colorado",
    "CT": "Connecticut",
    "DE": "Delaware",
    "DC": "District Of Columbia",
    "FL": "Florida",
    "GA": "Georgia",
    "GU": "Guam",
    "HI": "Hawaii",
    "ID": "Idaho",
    "IL": "Illinois",
    "IN": "Indiana",
    "IA": "Iowa",
    "KS": "Kansas",
    "KY": "Kentucky",
    "LA": "Louisiana",
    "ME": "Maine",
    "MD": "Maryland",
    "MA": "Massachusetts",
    "MI": "Michigan",
    "MN": "Minnesota",
    "MS": "Mississippi",
    "MO": "Missouri",
    "MT": "Montana",
    "NE": "Nebraska",
    "NV": "Nevada",
    "NH": "New Hampshire",
    "NJ": "New Jersey",
    "NM": "New Mexico",
    "NY": "New York",
    "NC": "North Carolina",
    "ND": "North Dakota",
    "OH": "Ohio",
    "OK": "Oklahoma",
    "OR": "Oregon",
    "PA": "Pennsylvania",
    "RI": "Rhode Island",
    "SC": "South Carolina",
    "SD": "South Dakota",
    "TN": "Tennessee",
    "TX": "Texas",
    "UT": "Utah",
    "VT": "Vermont",
    "VA": "Virginia",
    "WA": "Washington",
    "WV": "West Virginia",
    "WI": "Wisconsin",
    "WY": "Wyoming"
    }

    states = []

    for (var key in percentages)
    {
        states.push(all_states[key])
    }

    return states
}

function getListMax(list)
{
    var max = 0

    for (var i in list)
    {
        if (list[i] > max) {
            max = list[i]
        }
    }

    return max
}
