// graph percentage of cuisine/ambience in each state
function plotStates(div, percentages, type)
{
    var scl = [['0.0', 'rgb(49,54,149)'], ['0.111111111111', 'rgb(69,117,180)'],
    ['0.222222222222', 'rgb(116,173,209)'], ['0.333333333333', 'rgb(171,217,233)'],
    ['0.444444444444', 'rgb(224,243,248)'], ['0.555555555556', 'rgb(254,224,144)'],
    ['0.666666666667', 'rgb(253,174,97)'], ['0.777777777778', 'rgb(244,109,67)'],
    ['0.888888888889', 'rgb(215,48,39)'], ['1.0', 'rgb(165,0,38)']];

    var data = [{
          type: 'choropleth',
          locationmode: 'USA-states',
          locations: getDictKeys(percentages),
          z: getDictValues(percentages),
          //text: unpack(rows, 'state'),
          text: getStateNames(percentages),
          zmin: 0,
          colorscale: scl,
          colorbar: {
              title: 'Percentage',
              thickness: 30,
              autotic: true,
          },
          marker: {
              line: {
                  color: 'rgb(255,255,255)',
                  width: 2
              },
          }
      }];

      var layout = {
          title: type + ' Restaurants by State',
          width: 800,
          height: 500,
          geo: {
              scope: 'usa',
              showlakes: true,
              lakecolor: 'rgb(255,255,255)'
          }
      };
      Plotly.plot(div, data, layout, {showLink: false});
}
