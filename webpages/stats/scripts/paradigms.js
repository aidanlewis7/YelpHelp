function Item()
{
    this.item = null

    this.addToDocument = function()
    {
        document.body.appendChild(this.item)
    }
}

function Label()
{
    this.is_new = true

    this.createLabel = function(text, id)
    {
        this.item = document.createElement("p")
        var textLabel = document.createTextNode(text)

        this.item.appendChild(textLabel)
        this.item.setAttribute("id", id)
    }

    this.setText = function(text)
    {
        this.item.innerHTML = text
    }
}

function Button()
{
    this.createButton = function(text, id)
    {
        this.item = document.createElement("button")
        this.item.innerHTML = text
        this.item.setAttribute("id", id)
    }

    this.addClickEventHandler = function(handler, arg)
    {
        this.item.onmouseup = function(){ handler(arg) }
    }
}

function Div()
{
    this.createDiv = function(id)
    {
        this.item = document.createElement("div")
        this.item.setAttribute("id", id)
    }

    this.addItemToDiv = function(element)
    {
        this.item.appendChild(element)
    }
}

function Dropdown()
{
    this.createDropdown = function(dict, id)
    {
        this.item = document.createElement("select")

        this.id = id
        this.item.setAttribute("id", id)

        this.setValues(dict)
    }

    this.getValue = function()
    {
        var options = this.item

        var element = document.getElementById(this.id)

        var value = element.options[element.selectedIndex].value

        return value
    }

    this.reset = function()
    {
        var options = this.item

        var element = document.getElementById(this.id)

        element.options.length = 0
    }

    this.setValues = function(dict)
    {
        for (var key in dict)
        {
            var option = document.createElement("option")
            option.setAttribute("type", "dropdown")
            option.setAttribute("name", "rating")
            option.setAttribute("value", key)
            option.setAttribute("text", dict[key])

            option.innerHTML = capitalizeFirstLetter(dict[key])

            this.item.appendChild(option)
        }
    }

    this.storeDict = function(dict, topic)
    {
        this.cuisine = dict
        console.log("IN STORE DICT:", this.cuisine)
    }
}

// get all options for dropdown from webservice
function getOptions(div, dropdown, topic, port)
{
    var httpRequest = new XMLHttpRequest()

    url = getUrl(port, topic.toLowerCase() + "_options")

    httpRequest.open("GET", url, true)

    httpRequest.onload = function(e)
    {
        var topics = JSON.parse(httpRequest.responseText)[topic + 's']
        var topic_dict = {}

        for (var i in topics)
        {
            topic_dict[topics[i]] = topics[i]
        }

        dropdown.createDropdown(topic_dict, "topic_type_dict")

        div.addItemToDiv(dropdown.item)

    }

    httpRequest.onerror = function(e)
    {
        console.log(httpRequest.responseText)
    }

    httpRequest.send(null)
}


function updateOptions(dropdown, topic, port)
{
    var httpRequest = new XMLHttpRequest()

    url = getUrl(port, topic.toLowerCase() + "_options")

    httpRequest.open("GET", url, true)

    httpRequest.onload = function(e)
    {
        var topics = JSON.parse(httpRequest.responseText)[topic + 's']
        var topic_dict = {}

        for (var i in topics)
        {
            topic_dict[topics[i]] = topics[i]
        }

        dropdown.reset()
        dropdown.setValues(topic_dict)

    }

    httpRequest.onerror = function(e)
    {
        console.log(httpRequest.responseText)
    }

    httpRequest.send(null)
}

// on change function
function dropdownChanged(topic_dropdown, topic_type_dropdown, port)
{
    updateOptions(topic_type_dropdown, topic_dropdown.getValue(), port)
}

// called when user presses submit
function submit(objects)
{
    topic = objects[0].getValue()
    topic_type = objects[1].getValue()
    location_type = objects[2].getValue()
    port = objects[3]

    var grapher = prepareGraph()

    if (location_type == "state")
    {
        getPercentages(grapher, topic, topic_type, location_type, port)
    }
    else
    {
        getCoordinates(grapher, topic, topic_type, port)
    }
}

// find percentage of cuisine/ambience in each state
function getPercentages(grapher, topic, topic_type, location_type, port)
{
    var httpRequest = new XMLHttpRequest()
    url = getUrl(port, topic + "/all_percentages")

    params = {}

    var first_time = true;

    url += (first_time ? "?" : "&") + "location_type=" + location_type
    url += "&" + topic.toLowerCase() + "_type=" + topic_type

    console.log("URL: ", url)

    first_time = false

    httpRequest.open("GET", url, true)

    httpRequest.onload = function(e)
    {
        var percentages = JSON.parse(httpRequest.responseText).locations

        plotStates(grapher, percentages, capitalizeFirstLetter(topic_type))

    }

    httpRequest.onerror = function(e)
    {
        console.log(httpRequest.responseText)
    }

    httpRequest.send(null)
}

// get percentages plus coordinates to graph on map
function getCoordinates(div, topic, topic_type, port)
{
    var httpRequest = new XMLHttpRequest()
    url = getUrl(port, topic + "/all_locations")

    url += "?" + topic.toLowerCase() + "_type=" + topic_type

    console.log("URL: ", url)

    httpRequest.open("GET", url, true)

    httpRequest.onload = function(e)
    {
        var restaurants = JSON.parse(httpRequest.responseText).restaurants

        cities = {}

        for (var i in restaurants)
        {
            if (!(restaurants[i].city in cities)) {
                cities[restaurants[i].city] = {
                    "count": 1,
                    "latitude": restaurants[i].latitude,
                    "longitude": restaurants[i].longitude,
                    "ratings_sum": restaurants[i].stars
                }
            }
            else {
                cities[restaurants[i].city].count += 1
                cities[restaurants[i].city].ratings_sum += restaurants[i].stars
            }
        }

        names = []
        counts = []
        latitudes = []
        longitudes = []
        ratings = []

        for (var city in cities)
        {

            names.push(city)
            counts.push(cities[city].count)
            latitudes.push(cities[city].latitude)
            longitudes.push(cities[city].longitude)
            ratings.push(cities[city].ratings_sum / cities[city].count)
        }

        plotCities(div, names, counts, longitudes, latitudes, ratings, capitalizeFirstLetter(topic_type))

    }

    httpRequest.onerror = function(e)
    {
        console.log(httpRequest.responseText)
    }

    httpRequest.send(null)

}

function getUrl(port, category)
{
    return "http://student04.cse.nd.edu:" + port.toString() + "/" + category + "/"
}

function capitalizeFirstLetter(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}
