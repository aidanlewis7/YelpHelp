// graph percentage of cuisine/ambience by city
function plotCities(div, names, counts, longitudes, latitudes, ratings, topic_type)
{
    var scl = [['0.0', 'rgb(49,54,149)'], ['0.111111111111', 'rgb(69,117,180)'],
    ['0.222222222222', 'rgb(116,173,209)'], ['0.333333333333', 'rgb(171,217,233)'],
    ['0.444444444444', 'rgb(224,243,248)'], ['0.555555555556', 'rgb(254,224,144)'],
    ['0.666666666667', 'rgb(253,174,97)'], ['0.777777777778', 'rgb(244,109,67)'],
    ['0.888888888889', 'rgb(215,48,39)'], ['1.0', 'rgb(165,0,38)']];

    color = ["rgb(255,65,54)","rgb(133,20,75)","rgb(255,133,27)","lightgrey"],
    citySize = [],
    hoverText = [],
    scale = 4

    for ( var i = 0 ; i < counts.length; i++) {
        var currentSize = counts[i] / scale
        var currentText = names[i] + ": " + counts[i];
        citySize.push(currentSize);
        hoverText.push(currentText);
    }

    var data = [{
        type: 'scattergeo',
        locationmode: 'USA-states',
        lat: latitudes,
        lon: longitudes,
        hoverinfo: 'text',
        text: hoverText,
        marker: {
            size: citySize,
            line: {
                color: 'black',
                width: 2
            },
            colorscale: 'Greens',
            reversescale: true,
            cmin: 0,
            color: ratings,
            colorbar: {
                title: 'Average Rating'
            }
        }
    }];

    var layout = {
        title: topic_type + " Restaurants By City",
        showlegend: false,
        width: 800,
        height: 500,
        geo: {
            scope: 'usa',
            projection: {
                type: 'albers usa'
            },
            showland: true,
            landcolor: 'rgb(217, 217, 217)',
            subunitwidth: 1,
            countrywidth: 1,
            subunitcolor: 'rgb(255,255,255)',
            countrycolor: 'rgb(255,255,255)'
        },
    };

    Plotly.plot(div, data, layout, {showLink: false});
}
