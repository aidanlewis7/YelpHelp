function Item()
{
    this.item = null

    this.addToDocument = function()
    {
        document.body.appendChild(this.item)
    }
}

function Label()
{
    this.is_new = true

    this.createLabel = function(text, id)
    {
        this.item = document.createElement("p")
        var textLabel = document.createTextNode(text)

        this.item.appendChild(textLabel)
        this.item.setAttribute("id", id)
    }

    this.setText = function(text)
    {
        this.item.innerHTML = text
    }
}

function Button()
{
    this.createButton = function(text, id)
    {
        this.item = document.createElement("button")
        this.item.innerHTML = text
        this.item.setAttribute("id", id)
    }

    this.addClickEventHandler = function(handler, arg)
    {
        this.item.onmouseup = function(){ handler(arg) }
    }
}

function Div()
{
    this.createDiv = function(id)
    {
        this.item = document.createElement("div")
        this.item.setAttribute("id", id)
    }

    this.addItemToDiv = function(element)
    {
        this.item.appendChild(element)
    }
}
//
// function Form()
// {
// 	this.createForm = function(id)
// 	{
// 		this.item = document.createElement("")
// 		this.item.setAttribute("id", id)
// 	}
// 	this.addItemToDiv = function(element)
//     {
//         this.item.appendChild(element)
//     }
// }

function Form() {
	this.createForm = function(id, myFunction)
	{
		this.item = document.createElement("form")
		this.item.element1 = document.createElement("input");
	    this.item.element2 = document.createElement("input");
		this.item.element3 = document.createElement("input");
		this.item.id = id
	    this.item.method = "POST";
	    this.item.action = "http://student04.cse.nd.edu:51039/reviews/";

		//this.item.div1 = new Div();
		//this.item.div1.createDiv("div1");
		this.item.label1 = new Label();
		this.item.label1.createLabel("Restaurant", "l1");
		//this.item.div1.addItemToDiv(this.item.label1.item);
		//this.item.label1.addToDocument();
	    this.item.element1.type="text";
	    this.item.element1.name="restaurant";
	    this.item.appendChild(this.item.element1);
		//this.item.div1.addItemToDiv(this.item.element1);
		//document.body.appendChild(this.item.element1);

		//this.item.div2 = new Div();
		//this.item.div2.createDiv("div2");
		this.item.label2 = new Label();
		this.item.label2.createLabel("Review", "l2");
		//this.item.div1.addItemToDiv(this.item.label2.item);
		//this.item.label2.addToDocument();
	    this.item.element2.type="text";
	    this.item.element2.name="content";
	    this.item.appendChild(this.item.element2);
		//this.item.div2.addItemToDiv(this.item.element2);
		//document.body.appendChild(this.item.element2);

		//this.item.div3 = new Div();
		//this.item.div3.createDiv("div3");
		this.item.label3 = new Label();
		this.item.label3.createLabel("Rating", "l3");
		//this.item.div1.addItemToDiv(this.item.label3.item);
		//this.item.label3.addToDocument();
		this.item.element3.type="number";
	    this.item.element3.name="rating";
	    this.item.appendChild(this.item.element3);
		//this.item.div3.addItemToDiv(this.item.element3);
		//document.body.appendChild(this.item.element3);

		this.item.button = new Button()
		this.item.button.createButton("Submit", "myButton");
		this.item.button.addClickEventHandler(myFunction)
		document.body.appendChild(this.item);
		this.item.button.addToDocument();

		// this.item.button.type="button";
		// this.item.button.onclick=myFunction;
		// this.item.button.value="Submit Form";

		//this.item.submit();
	    //document.body.appendChild(this.item);
	}
    // var form = document.createElement("form");
    // var element1 = document.createElement("input");
    // var element2 = document.createElement("input");
	// var element2 = document.createElement("input");
    //
	// form.id = "myForm"
    // form.method = "POST";
    // form.action = "http://student04.cse.nd.edu:51039/reviews/";
    //
    // element1.type="text";
    // element1.name="restaurant";
    // form.appendChild(element1);
    //
    // element2.value=text;
    // element2.name="content";
    // form.appendChild(element2);
    //
	// element3.type="number";
    // element3.name="rating";
    // form.appendChild(element3);
    //
    // document.body.appendChild(form);
    // form.submit();
}

Button.prototype = new Item()
Label.prototype = new Item()
Div.prototype = new Item()
Form.prototype = new Item()