// parent class to make appending easy
function Item()
{
    this.item = null

    this.addToDocument = function()
    {
        document.body.appendChild(this.item)
    }
}

function Label()
{
    this.is_new = true

    this.createLabel = function(text, id)
    {
        this.item = document.createElement("p")
        var textLabel = document.createTextNode(text)

        this.item.appendChild(textLabel)
        this.item.setAttribute("id", id)
    }

    this.setText = function(text)
    {
        this.item.innerHTML = text
    }
}


function Button()
{
    this.createButton = function(text, id)
    {
        this.item = document.createElement("button")
        this.item.setAttribute("id", id)
        this.item.innerHTML = text
    }

    this.addClickEventHandler = function(handler, arg)
    {
        this.item.onmouseup = function(){ handler(arg) }
    }
}

function Image() {
    this.createImage = function(text, id) {
        this.item = document.createElement("IMG")
        this.item.setAttribute("id", id)

    }
    this.setImage = function(path) {
        this.item.setAttribute("src", path)
    }
}


function Div()
{
    this.createDiv = function(id)
    {
        this.item = document.createElement("div")
        this.item.setAttribute("id", id)
    }

    this.addItemToDiv = function(element)
    {
        this.item.appendChild(element)
    }
}

function Input()
{
    this.createInput = function(name, type)
    {
        this.item = document.createElement("input")
        this.item.setAttribute("type", type)
        this.item.setAttribute("name", name)
    }

    this.getValue = function()
    {
        return this.item.value
    }
}

// dynamically generate an ordered list of each favorite restaurant
function display_favorites()
{
    try {
        var element = document.getElementById("favsDiv")
        element.parentNode.removeChild(element)
    }
    catch (e) {
        var temp = {}
    }

    var div = new Div()
    div.createDiv("favsDiv")

    var label = new Label()
    label.createLabel("Favorites", "favsLabel")
    div.addItemToDiv(label.item)

    // perform HTTP GET request
    var xhr = new XMLHttpRequest()
    xhr.open("GET", "http://student04.cse.nd.edu:51039/favorites/", true)

    xhr.onload = function(e) {
        resp = JSON.parse(xhr.responseText)
        restaurants = resp.restaurants

        // create ordered list for each restaurant stored as a favorite
        for (var i in restaurants)
        {
            restaurant = restaurants[i]

            var list = document.createElement("ol")
            list.setAttribute("type", "1")
            // add "li" elements to the list for each restaurant attribute
            addItem(restaurant, list)

            // add list to div, and div to document
            div.addItemToDiv(list)
        }

        div.addToDocument()

    }

    xhr.onerror = function(e) {
        console.error(xhr.statusText)
    }

    xhr.send(null)
}

// create list elements for each restaurant attribute
function addItem(restaurant, list){
    var r = "Restaurant: " + restaurant.restaurant
    var l = "Location: " + restaurant.location
    var review = "Review: " + restaurant.review

    // restaurant item
    var r_li = document.createElement("li");
    //console.log("r_li", r_li)
    r_li.setAttribute('id', r);
    r_li.appendChild(document.createTextNode(r));
    list.appendChild(r_li);

    // location item
    var l_li = document.createElement("li")
    l_li.setAttribute('id', l);
    l_li.appendChild(document.createTextNode(l));
    list.appendChild(l_li);

    // review item
    var review_li = document.createElement("li")
    review_li.setAttribute('id', review);
    review_li.appendChild(document.createTextNode(review));
    list.appendChild(review_li);
}


function createLabel(div, text, id, input_id)
{
    label = new Label();
    label.createLabel(text, id);
    div.addItemToDiv(label.item)

    input = new Input();
    input.createInput(input_id, "text")
    div.addItemToDiv(input.item)

    return input
}

// prepare data to submit for post request
function submit(objects)
{
    port = objects[0]
    resInput = objects[1]
    locInput = objects[2]
    revInput = objects[3]

    data = {"restaurant": resInput.getValue(), "location": locInput.getValue(), "review": revInput.getValue()}

    postRestaurant(port, data)
}

// send HTTP POST request
function postRestaurant(port, data)
{
    var httpRequest = new XMLHttpRequest()

    var url = getUrl(port, "favorites")
    var params = ""
    var first_time = true;

    // add variables to url string
    for (var key in data)
    {
        params += (first_time ? "" : "&") + key + "=" + data[key]
        first_time = false
    }

    httpRequest.open("POST", url, true)

    httpRequest.onload = function(e)
    {
        var response = JSON.parse(httpRequest.responseText)

        console.log("POST: ", response)

        display_favorites()

    }

    httpRequest.onerror = function(e)
    {
        console.log(httpRequest.responseText)
    }

    httpRequest.send(params.toString())
}

// generate URL
function getUrl(port, category)
{
    return "http://student04.cse.nd.edu:" + port.toString() + "/" + category + "/"
}

// send HTTP GET request to get the favorites 
function getFavorites()
{
    var httpRequest = new XMLHttpRequest()
    var url = getUrl(port, "favorites")

    httpRequest.open("GET", url, true)

    httpRequest.onload = function(e)
    {
        var response = JSON.parse(httpRequest.responseText)

        console.log("GET RESPONSE: ", response)
    }

    httpRequest.onerror = function(e)
    {
        console.log(httpRequest.responseText)
    }

    httpRequest.send(null)
}
