var port = 51039

// inherit from item
Label.prototype = new Item()
Button.prototype = new Item()
Div.prototype = new Item()
Input.prototype = new Item()

// create sample
sample = {"restaurant": "Torchy's Tacos", "location": "TX", "review": "Delicious!"}
postRestaurant(port, sample)

// create favorites label
var add_favorite_label = new Label()
add_favorite_label.createLabel("Add a Favorite Restaurant!", "add_favorite_label")
add_favorite_label.addToDocument()

// create the form
formDiv = new Div()
formDiv.createDiv("inputDiv")

// create form inputs
resInput = createLabel(formDiv, "Restaurant", "resLabel", "resInput")
locInput = createLabel(formDiv, "Location", "locLabel", "locInput")
revInput = createLabel(formDiv, "Review", "revLabel", "revInput")

formDiv.addToDocument()

// create submit button 
submit_button = new Button()
submit_button.createButton("Submit", "favorites_submit_button")
submit_button.addClickEventHandler(submit, [port, resInput, locInput, revInput])
submit_button.addToDocument()
