var port = 51039
var button_path = "/Users/jessicahardey/CSECourses/Paradigms/YelpHelp/webpages/"


Input.prototype = new Item()
ImageButton.prototype = new Item()
Div.prototype = new Item()

// Create YelpHelp Logo
var yelp_help_input = new Input()
yelp_help_input.createImageInput("yelp_help_input_image", "../../images/yelp_help.v2.png")

var yelp_help_button = new ImageButton()
yelp_help_button.createImageButton("https://www.yelp.com", yelp_help_input)

// Create Quiz Button
var quiz_input = new Input()
quiz_input.createImageInput("quiz_input_image", "../../images/quiz.v2.png")

var quiz_button = new ImageButton()
quiz_button.createImageButton(button_path + "quiz/index.html", quiz_input)

// Create Favorites Button
var favorites_input = new Input()
favorites_input.createImageInput("favorites_input_image", "../../images/favorites.v2.png")

var favorites_button = new ImageButton()
favorites_button.createImageButton(button_path + "favorites/index.html", favorites_input)

// Create Add&Review Button
var add_and_review_input = new Input()
add_and_review_input.createImageInput("add_and_review_input_image", "../../images/add_and_review.v2.png")

var add_and_review_button = new ImageButton()
add_and_review_button.createImageButton(button_path + "AnR/index.html", add_and_review_input)

// Create stats Button
var stats_input = new Input()
stats_input.createImageInput("stats_input_image", "../../images/stats.v2.png")

var stats_button = new ImageButton()
stats_button.createImageButton(button_path + "stats/index.html", stats_input)


// add buttons to div
var button_div = new Div()
button_div.createDiv("button_div")

var stats_button_div = new Div()
stats_button_div.createDiv("stats_button_div")
stats_button_div.addItemToDiv(stats_button)
stats_button_div.addToDocument()

var favorites_button_div = new Div()
favorites_button_div.createDiv("favorites_button_div")
favorites_button_div.addItemToDiv(favorites_button)
favorites_button_div.addToDocument()

var yelp_help_button_div = new Div()
yelp_help_button_div.createDiv("yelp_help_button_div")
yelp_help_button_div.addItemToDiv(yelp_help_button)
yelp_help_button_div.addToDocument()

var add_and_review_button_div = new Div()
add_and_review_button_div.createDiv("add_and_review_button_div")
add_and_review_button_div.addItemToDiv(add_and_review_button)
add_and_review_button_div.addToDocument()

var quiz_button_div = new Div()
quiz_button_div.createDiv("quiz_button_div")
quiz_button_div.addItemToDiv(quiz_button)
quiz_button_div.addToDocument()

// add div to overall button div 
button_div.addItemToDiv(stats_button_div)
button_div.addItemToDiv(favorites_button_div)
button_div.addItemToDiv(yelp_help_button_div)
button_div.addItemToDiv(add_and_review_button_div)
button_div.addItemToDiv(quiz_button_div)
button_div.addToDocument()
