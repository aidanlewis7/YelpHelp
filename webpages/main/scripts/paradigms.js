function Item()
{
    this.item = null

    this.addToDocument = function()
    {
        document.body.appendChild(this.item)
    }
}

function Label()
{
    this.is_new = true

    this.createLabel = function(text, id)
    {
        this.item = document.createElement("p")
        var textLabel = document.createTextNode(text)

        this.item.appendChild(textLabel)
        this.item.setAttribute("id", id)
    }

    this.setText = function(text)
    {
        this.item.innerHTML = text
    }
}

function ImageButton()
{
    this.createImageButton = function(link, image)
    {
        this.item = document.createElement("a")
        this.item.setAttribute("href", link)
        this.item.appendChild(image.item)
    }
}

function Input()
{
    this.createImageInput = function(id, path)
    {
        this.item = document.createElement("input")
        this.item.setAttribute("type", "image")
        this.item.setAttribute("id", id)
        this.item.setAttribute("src",path)

    }
}

function Image() {
    this.createImage = function(text, id) {
        this.item = document.createElement("IMG")
        this.item.setAttribute("id", id)

    }
    this.setImage = function(path) {
        this.item.setAttribute("src", path)
    }
}


function Div()
{
    this.createDiv = function(id)
    {
        this.item = document.createElement("div")
        this.item.setAttribute("id", id)
    }

    this.addItemToDiv = function(element)
    {
        this.item.appendChild(element.item)
    }
}
