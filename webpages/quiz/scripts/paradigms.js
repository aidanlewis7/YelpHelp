function Item()
{
    this.item = null

    this.addToDocument = function()
    {
        document.body.appendChild(this.item)
    }
}

function Label()
{
    this.is_new = true

    this.createLabel = function(text, id)
    {
        this.item = document.createElement("p")
        var textLabel = document.createTextNode(text)

        this.item.appendChild(textLabel)
        this.item.setAttribute("id", id)
    }

    this.setText = function(text)
    {
        this.item.innerHTML = text
    }
}

function Button()
{
    this.createButton = function(text, id)
    {
        this.item = document.createElement("button")
        this.item.innerHTML = text
        this.item.setAttribute("id", id)
    }

    this.addClickEventHandler = function(handler, arg)
    {
        this.item.onmouseup = function(){ handler(arg) }
    }
}

function MultipleChoice()
{
    this.createMultipleChoice = function(id, choices)
    {
        this.item = document.createElement("form")

        this.id = id
        this.item.setAttribute("id", id)

        // cycle through choices and add each option
        for (var i in choices)
        {
            var div = document.createElement("div")

            var label = document.createElement("label")
            label.innerHTML = choices[i]

            var choice = document.createElement("input")
            choice.setAttribute("type", "radio")
            choice.setAttribute("name", this.id)
            choice.setAttribute("value", choices[i])
            choice.innerHTML = choices[i] // set text

            label.appendChild(choice) // add to label
            div.appendChild(label) // add label to div

            this.item.appendChild(div) // add div to multiple choice section
        }
    }

    this.getValue = function() // get multiple choice value (which is selected)
    {
        var radios = this.item.elements[this.id];

        for (var i=0, len=radios.length; i<len; i++) {
            if ( radios[i].checked ) {
                return radios[i].value;
            }
        }
    }
}

function Div()
{
    this.createDiv = function(id)
    {
        this.item = document.createElement("div")
        this.item.setAttribute("id", id)
    }

    this.addItemToDiv = function(element)
    {
        this.item.appendChild(element)
    }
}


function submit(objects)
{
    mcs = objects[0]
    port = objects[1]
    result_div = objects[2]
    name_label = objects[3]
    location_label = objects[4]

    var httpRequest = new XMLHttpRequest()
    url = getUrl(port, "quiz")

    var first_time = true;

    // add variables to url string
    for (var key in mcs)
    {
        url += (first_time ? "?" : "&") + key.toLowerCase() + "=" + mcs[key].getValue()
        first_time = false
    }

    httpRequest.open("GET", url, true)

    httpRequest.onload = function(e)
    {
        var response = JSON.parse(httpRequest.responseText)

        if (response["result"] == "success") { // get match
            var name_text = response["name"]
            var location_text = response["city"] + ", " + response["state"]
        }
        else { // no match
            var name_text = "Sorry, no restaurants match those choices :("
            var location_text = ""
        }

        name_label.setText(name_text)
        location_label.setText(location_text)

        // only add label with restaurant on it first time
        if (name_label.is_new) {
            result_div.addToDocument()
            name_label.is_new = false
        }
    }

    httpRequest.onerror = function(e)
    {
        console.log(httpRequest.responseText)
    }

    httpRequest.send(null)
}


function getUrl(port, category)
{
    return "http://student04.cse.nd.edu:" + port.toString() + "/" + category + "/"
}


function get_mc_data(mcs, key, port, search_div)
{
    var httpRequest = new XMLHttpRequest()
    url = getUrl(port, key.toLowerCase() + "_options")

    httpRequest.open("GET", url, true)

    // get state options from web service
    httpRequest.onload = function(e)
    {

        var response = JSON.parse(httpRequest.responseText)
        var choices = response[key.toLowerCase() + 's']

        if (key.toLowerCase() == "state")
        {
            var updated_choices = []

            for (var i in choices)
            {
                if (choices[i] != "NY" && choices[i] != "WA" && choices[i] != "CA" && choices[i] != "SC")
                {
                    updated_choices.push(choices[i])
                }
            }

            choices = updated_choices
        }

        create_mc(mcs, [key], choices, search_div)
    }

    httpRequest.onerror = function(e)
    {
        console.log(httpRequest.responseText)
    }

    httpRequest.send(null)
}

// create each multiple choice question
function create_mc(mcs, key, choices, search_div)
{
    var div = new Div()
    div.createDiv("mc_div_" + key[0])

    var label = new Label()

    var text = key[0] + (key.length == 1 ? "" : key[1])

    label.createLabel(text, "mc_label_" + key[0])
    div.addItemToDiv(label.item)

    mcs[key[0]] = new MultipleChoice()
    mcs[key[0]].createMultipleChoice(key[0], choices)
    mcs[key[0]].addToDocument()

    div.addItemToDiv(mcs[key[0]].item)
    div.addToDocument()

    search_div.addToDocument() // add search button only after all multiple choice in place
}
