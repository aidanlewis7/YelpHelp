var initial_mid = 456
var port = 51039

Label.prototype = new Item()
Button.prototype = new Item()
MultipleChoice.prototype = new Item()
Div.prototype = new Item()

var mcs = {}

// Search Button
var search_button = new Button()
search_button.createButton("Search", "search_button")

// create div for search button
var search_div = new Div()
search_div.createDiv("search_div")
search_div.addItemToDiv(search_button.item)

// make all multiple choice questions
get_mc_data(mcs, "State", port, search_div)
get_mc_data(mcs, "Cuisine", port, search_div)
get_mc_data(mcs, "Ambience", port, search_div)
create_mc(mcs, ["Rating", " at least..."], ["0", "1", "2", "3", "4", "5"], search_div)

// create result div (contains name of restaurant/location that matches your choices)
var result_div = new Div()
result_div.createDiv("result_div")

// contains chosen restaurant's name
var name_label = new Label()
name_label.createLabel("", "name_label")
result_div.addItemToDiv(name_label.item)

// contains chosen location
var location_label = new Label()
location_label.createLabel("", "location_label")
result_div.addItemToDiv(location_label.item)

// handle submit
search_button.addClickEventHandler(submit, [mcs, port, result_div, name_label, location_label])
