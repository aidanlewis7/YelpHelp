import unittest

from _yelp_database import _yelp_database

from cuisine import cuisine
from ambience import ambience
from visited_restaurant import visitedRestaurant
from review import review
from quiz import quiz

class TestYelp(unittest.TestCase):

    yelp = _yelp_database()
    ambience = ambience(yelp)
    cuisine = cuisine(yelp)
    visited_restaurant = visitedRestaurant(yelp)
    review = review(yelp)
    quiz = quiz(cuisine, ambience)

    states = [ "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA",
          "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
          "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
          "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC",
          "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]

    def test_load_data(self):

        for v in self.yelp.data.values():
            self.assertTrue("Restaurants" in v["categories"])
            self.assertTrue(v["state"] in self.states)


    def test_get_state(self):

        test_states = ["AZ", "NV", "CA", "NY", "PA"]

        for s in test_states:
            self.assertTrue(s in self.yelp.get_states())

        self.assertTrue("ND" not in self.yelp.get_states())

    # ----------------------- AMBIENCE ---------------------------------


    def test_ambience_get_options(self):

        ambiences = self.ambience.get_options()
        test_ambiences = ["romantic", "casual", "hipster"]

        for a in test_ambiences:
            self.assertTrue(a in ambiences)


    def test_ambience_get_all_locations(self):

        atmosphere = "hipster"

        restaurants = self.ambience.get_all_locations(atmosphere)

        for r in restaurants:
            self.assertTrue(r['attributes']['Ambience'][atmosphere])


    def test_ambience_get(self):

        state = "AZ"
        atmosphere = "hipster"

        restaurants = self.ambience.get("state", state, atmosphere)

        for v in restaurants:
            self.assertEqual(v["state"], state)
            self.assertTrue(v['attributes']['Ambience'][atmosphere])


    def test_ambience_get_percentage(self):

        states = ["PA", "WI", "NY"]
        atmospheres = ["classy", "touristy", "divey"]

        for state in states:
            for atmosphere in atmospheres:
                percentage_1 = self.ambience.get_percentage("state", state, atmosphere)
                restaurants = self.ambience.get("state", state, atmosphere)
                percentage_2 = 100 * len(restaurants) / float(self.yelp.locations["state"][state])
                self.assertEqual(percentage_1, percentage_2)


    def test_ambience_get_all_percentages(self):

        ambience = "upscale"

        locations = self.ambience.get_all_percentages("city", ambience)

        for city in self.yelp.locations["city"].keys():
            num = self.ambience.get_percentage("city", city, ambience)

            self.assertEqual(num, locations[city])


    # ----------------------- CUISINE ---------------------------------

    def test_cuisine_get_options(self):

        cuisines = self.cuisine.get_options()
        test_cuisines = ["Italian", "Mexican", "Chinese"]

        for c in test_cuisines:
            self.assertTrue(c in cuisines)


    def test_cuisine_get_all_locations(self):

        test_cuisine = "Italian"

        restaurants = self.cuisine.get_all_locations(test_cuisine)

        for r in restaurants:
            self.assertTrue(test_cuisine in r['categories'])


    def test_cuisine_get(self):

        city = "Charlotte"
        cuisine = "Italian"

        restaurants = self.cuisine.get("city", city, cuisine)

        for v in restaurants:
            self.assertEqual(v["city"], city)
            self.assertTrue(cuisine in v['categories'])


    def test_cusine_get_percentage(self):

        states = ["NV", "CA", "IL"]
        cuisines = ["Mexican", "Irish", "Polish"]

        for state in states:
            for cuisine in cuisines:
                percentage_1 = self.cuisine.get_percentage("state", state, cuisine)
                restaurants = self.cuisine.get("state", state, cuisine)
                percentage_2 = 100 * len(restaurants) / float(self.yelp.locations["state"][state])
                self.assertEqual(percentage_1, percentage_2)


    def test_cuisine_get_all_percentages(self):

        cuisine = "Chinese"

        locations = self.cuisine.get_all_percentages("city", cuisine)

        for city in self.yelp.locations["city"].keys():
            num = self.cuisine.get_percentage("city", city, cuisine)

            self.assertEqual(num, locations[city])


    # ---------------------------- QUIZ ------------------------------------

    def test_get_quiz_result(self):

        state = "AZ"
        rating = 3
        cuisine = "Mexican"
        ambience = "romantic"

        result = self.quiz.get_quiz_result(state, rating, cuisine, ambience)

        self.assertEqual(state, result["state"])
        self.assertTrue(result["stars"] >= rating)
        self.assertTrue(cuisine in result["categories"])
        self.assertTrue(ambience in result["attributes"]["Ambience"])

    # --------------------------- VISITED ----------------------------------

    def test_match_data(self):
        location1 = "NV"
        location2 = "TX"
        location3 = "Las Vegas"
        restaurant1 = "Quark's Bar & Restaurant"
        restaurant2 = "Torchy's Tacos"

        match1 = self.visited_restaurant.match_data(restaurant1, location1)
        match2 = self.visited_restaurant.match_data(restaurant2, location2)
        match3 = self.visited_restaurant.match_data(restaurant1, location3)

        self.assertEqual(match1, True)
        self.assertEqual(match2, False)
        self.assertEqual(match3, True)

    def test_add_restaurant(self):
        self.assertEqual(len(self.visited_restaurant.my_restaurants), 0)
        self.visited_restaurant.add_restaurant("Quark's Bar & Restaurant", "NV", "The food was good", True)
        self.visited_restaurant.add_restaurant("Torchy's Tacos", "TX", "Excellent food", True)
        self.visited_restaurant.add_restaurant("Torchy's Tacos", "TX", "Excellent food", True)

        self.assertEqual(self.visited_restaurant.my_restaurants[0], { "restaurant" : "Quark's Bar & Restaurant", "location" : "NV" , "review" : "The food was good", "visited" : True})
        self.assertEqual(self.visited_restaurant.my_restaurants[1], { "restaurant" : "Torchy's Tacos", "location" : "TX", "review" : "Excellent food", "visited" : True} )
        self.assertEqual(len(self.visited_restaurant.my_restaurants), 2)

    def test_ambience_get_ratings(self):
        is_correct = True
        states = ["PA", "WI", "NY"]
        atmospheres = ["classy", "touristy", "divey"]
        rating = 3.0

        for state in states:
            for atmosphere in atmospheres:
                restaurants, ratings = self.ambience.get_ratings("state", state, atmosphere, rating)

                for r in ratings:
                    if r < rating:
                        is_correct = False
                #print(restaurants)
                self.assertEqual(is_correct, True)

    def test_cuisine_get_ratings(self):
        is_correct = True
        states = ["NV", "CA", "IL"]
        cuisines = ["Mexican", "Irish", "Polish"]
        rating = 3.0

        for state in states:
            for cuisine in cuisines:
                restaurants, ratings = self.cuisine.get_ratings("state", state, cuisine, rating)

                for r in ratings:
                    if r < rating:
                        is_correct = False

                self.assertEqual(is_correct, True)

    def test_post_review(self):
        test_post = {"restaurant": "XD's Restaurant", "content": "It was awful!",  "rating": 1}
        test_reviews = self.review.post_review(test_post)
        self.assertEqual(test_reviews[0], test_post)
		# self.review.post_review(test_post)
		# somehow check if json file is updated, or just leave the above test

    def test_post_new_restaurant(self):
        # test1 = "Subway"
        # test2 = "LOL"
        # bool1 = self.review.post_new_restaurant(test1)
        # bool2 = self.review.post_new_restaurant(test2)
        # self.assertEqual(bool1, True)
        # self.assertEqual(bool2, False)

        test_post = {"restaurant": "XD's Restaurant", "content": "It was awful!",  "rating": 1}
        test_reviews = self.review.post_new_restaurant(test_post)
        self.assertEqual(test_reviews[0], test_post)
		# test_post = {"restaurant": "XD's Restaurant"}
		# test_post should be false or some like null rest name
        # test_reviews = self.review.post_review(test_post)
        # self.assertEqual(test_reviews[0], test_post)

    def test_delete_restaurant(self):
        self.visited_restaurant.add_restaurant("Quark's Bar & Restaurant", "NV", "The food was good", True)
        self.visited_restaurant.add_restaurant("Torchy's Tacos", "TX", "Excellent food", True)
        self.assertEqual(len(self.visited_restaurant.my_restaurants), 2)

        self.visited_restaurant.delete_restaurant("Torchy's Tacos", "TX")
        self.assertEqual(len(self.visited_restaurant.my_restaurants), 1)
        self.assertEqual(self.visited_restaurant.my_restaurants[0]['restaurant'], "Quark's Bar & Restaurant")

if __name__ == "__main__":
    unittest.main()
