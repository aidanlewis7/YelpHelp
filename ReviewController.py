# visited_restaurant_controller

import cherrypy
import re, json
from _yelp_database import _yelp_database
from review import review
import os

class ReviewController(object):
    def __init__(self, r):
        self.r = r

    def POST_TO_REVIEWS(self, restaurant, content, rating):
        output = {'result' : 'success'}
        #the_body = cherrypy.request.body.read().decode()

        try:
            #the_body = json.loads(the_body)
            # restaurant = the_body['restaurant']
            # content = the_body['content']
            # rating = the_body['rating']
            review = {'restaurant': restaurant, 'content': content, 'rating': rating}

            self.r.post_review(review)
        except:
            output['result'] = 'error'

        return json.dumps(output)

    def GET_REVIEWS(self):
        output = {'reviews' : [], 'result' : 'success'}

        try:
            for r in self.r.get_reviews():
                review = {}
                review['restaurant'] = r['restaurant']
                review['content'] = r['content']
                review['rating'] = int(r['rating'])
                output['reviews'].append(review)
        except:
            output['result'] = 'error'
            output['reviews'].append({'result' : 'error'})

        return json.dumps(output)
