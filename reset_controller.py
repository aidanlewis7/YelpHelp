import cherrypy
import re, json
import os
from _yelp_database import _yelp_database

class ResetController(object):
    def __init__(self, ydb):
           self.ydb = ydb
           self.ydb.load_data()


    def RESET(self):
        output = {'result' : 'success'}

        try:
            self.ydb.reset()
            self.ydb.load_data()

        except:
            output['result'] = 'error'

        return json.dumps(output)
