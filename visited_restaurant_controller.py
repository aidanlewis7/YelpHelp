# visited_restaurant_controller

import cherrypy
import re, json
from _yelp_database import _yelp_database
from visited_restaurant import visitedRestaurant
import os

class VisitedRestaurantController(object):
    def __init__(self, vr):
        self.vr = vr

    def POST_TO_FAVORITES(self):
        output = {'result' : 'success'}

        the_body = cherrypy.request.body.read().decode() # get the data to post

        try:
            # load the data into variables to add to database
            pairs = the_body.split("&")

            restaurant = pairs[0].split('=')[1]
            location = pairs[1].split('=')[1]
            review = pairs[2].split('=')[1]
            visited = True

            # use api function to add user input to favorite restaurants database
            self.vr.add_restaurant(restaurant, location, review, visited)
        except:
            output['result'] = 'error'

        return json.dumps(output)

    def GET_FAVORITES(self):

        output = {'restaurants' : [], 'result' : 'success'}

        try:
            # loop through saved restaurants and append to output
            for r in self.vr.get_favorite_restaurants():
                restaurant = {}
                restaurant['restaurant'] = r['restaurant']
                restaurant['location'] = r['location']
                restaurant['review'] = r['review']
                output['restaurants'].append(restaurant)
        except:
            output['result'] = 'error'
            output['restaurant'].append({'result' : 'error'})

        return json.dumps(output)

    def DELETE_FAVORITE(self):
        output = {'result' : 'success'}
        the_body = cherrypy.request.body.read().decode()

        try:
            the_body = json.loads(the_body)
            restaurant = the_body['restaurant']
            location = the_body['location']
            self.vr.delete_restaurant(restaurant, location)

        except:
            output['result'] = 'error'

        return json.dumps(output)
