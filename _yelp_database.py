import json

from collections import defaultdict

class _yelp_database:

    def __init__(self):
        self.data, self.locations = self.load_data()

    # --------------------------- load_data ----------------------------------------

    def is_us_restaurant(self, details, us_states):
        # if establishment is a restaurant and is in United States
        return "categories" in details and "Restaurants" in details["categories"] \
        and "state" in details and details["state"] in us_states


    def load_data(self):

        # States represented in dataset: OH, IL, AZ, CA, WA, SC, NY, PA, WI, NC, NV

        # Prominent cities included in dataset: Las Vegas, Phoenix, Charlotte, Pittsburgh,
        # Scottsdale, Cleveland, Madison, Henderson, Chandler, Concord,
        # Champaign, Peoria, etc.

        yelp_dict = defaultdict(dict)

        us_states = [ "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA",
              "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD",
              "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ",
              "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC",
              "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]

        # dictionary of dictionaries
        locations = defaultdict(lambda : defaultdict(int))

        # each line in file contains JSON for one restaurant
        for line in open("business.json"):

            # load JSON for restaurant from file
            details = json.loads(line)

            # if in United States and a restaurant
            if self.is_us_restaurant(details, us_states):
                bid = details["business_id"]
                yelp_dict[bid] = details # key is business_id

                # get restaurant count for each region
                locations["state"][yelp_dict[bid]["state"]] += 1
                locations["city"][yelp_dict[bid]["city"]] += 1

        return [yelp_dict, locations]

    def reset(self):
        self.data.clear()
        self.locations.clear()


    def get_states(self):
        return list(self.locations["state"].keys())


    # ------------------------------- utility functions --------------------------------

    def sort_dict_by_value(self, dict): # returns list of keys sorted by value
        return sorted(dict, key=dict.get)[::-1]
