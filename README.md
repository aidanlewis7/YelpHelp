Yelp Help

Members: Jessica Hardey, Paul Kwak, Aidan Lewis

Description:

API Guide

An API for using Yelp data to see how the prominence of different cultures and different dining experiences vary across different regions of the U.S.

This API can and should be used to generate statistics about restaurants in specified locations as well as to provide suggested restaurants based on user specifications.

This API loads data based on the following states and cities as provided by yelp.com:

States represented in dataset: OH, IL, AZ, CA, WA, SC, NY, PA, WI, NC, NV

Prominent cities included in dataset: Las Vegas, Phoenix, Charlotte, Pittsburgh, Scottsdale, Cleveland, Madison, Henderson, Chandler,Concord, Champaign, Peoria, etc.

The yelp database class loads the JSON file containing restaurant information for all the restaurants in the database. The information is placed into a dictionary with the business id as a key, and the details of the restaurant as the value. Also returned is a locations dictionary that returns a count of the number of restaurants in each region.

The ambience class can return all the restaurants in a particular region with a specific ambience (such as romantic or hipster), and can return the percentage of all restaurants in a region with this ambience. Further, it can return a list of regions ranked by the prominence of this ambience type, and can return each restaurant with this ambience that is rated above a specified number of stars. The cuisine class has all these features, but for a specific cuisine type (such as Mexican, Japanese, Italian, etc.).

The review class will be in charge of post requests if the user wants to
add a review to the restaurant or if they see a new restaurant not recognized
by Yelp's API data. It was designed to keep in mind that the data inputs would
come from the client side and be sent to the server as a POST request.


Web Service Guide

Our web service is run on port 51039.

The web service takes advantage of the ambience class of the API with GET methods
that allow a user to access all the information returned by the ambience part of
the API as described above. The web service allows the same for the cuisine class,
so that a user can use GET methods to utilize the functionality of the API.

The web service also uses the visited_restaurant class to create a "favorites" feature. This includes GET and POST method that allow the user to add a restaurant to their list of favorites and whether or not they've visited it (the POST) and also to get the list of all their favorite restaurants (the GET).

Additionally, the web service uses the review class to let users add in new restaurants
and add their own reviews and ratings of the new place. This includes a POST method that allow
the user to send a review to the server, and a GET method to just check to see if the server
actually received this data.

The web service also has a reset method, which is a PUT, that resets all the yelp data.


Client Guide

Statistics: This page utilizes the cuisine and ambience controllers in order to get data for the amount of restaurants in a certain region with a given cuisine or ambience. The user can select whether they would like to graph cuisine or ambience, and based on this, can choose which cuisine or ambience they wish to graph. After, they choose whether to graph by state or by city. When graphing by state, a map of the US will pop up with the percentage of the selected cuisine/ambience in each state in the data set. The color of the state will be correlated to its percentage of that restaurant. When city is selected, the amount of a given restaurant in each city is graphed on the US map.

Quiz: This page also utilizes the cuisine and ambience controllers, as well as the quiz controller. The user input the minimum rating, state, cuisine, and ambience they want for a restaurant, and then the output will be a random restaurant that matches each of these attributes.

Add & Review: This page calls upon the review side of the server, and asks that the user
inputs three things, a restaurant name, a review, and a rating from 1 to 5.
Unfortunately, it defaults the form sent to the url, and requires two backs to get
back to the home page.

Favorites: This page calls on the favorites extension of the server and takes three pieces of user input: restaurant name, restaurant location (either city or state), and the user's review. Upon submission of these inputs, the user sees this restaurant added to a list of their favorites, which is automatically output to the screen.


Steps to Running the Web Service:
(1) Navigate to final_project/webservice
(2) Type in the command line, 'python3 main.py'
(3) The server is now running!
(4) To get to the client, type in a web browser,
'http://student04.cse.nd.edu/jhardey/b0H32vv/final-project/YelpHelp/webpages/main/'
(5) This displays the home page, with each image including the middle going to
unique pages in the client.
(6) The back arrow is needed to get back to the home page for each page entered, and twice in
add and review, and favorites sections.
(7) Have fun!
