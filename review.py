from _yelp_database import _yelp_database
import json

class review:

    def __init__(self, database):
        self.reviews = list()
        self.newR = list()
        self.database = database
        self.data = database.data
        self.locations = database.locations

    def post_review(self, review):
        # hard coded values, that will eventually match up to client side form areas
        # easily can change input of post_review to include json obj sent from client as post
        # Will not be hard-coded once client POST is available
        #reviews = []
        # user_review = dict()
        # user_review['restaurant'] = "XD's Restaurant"
        # user_review['content'] = "It was awful!"
        # user_review['rating'] = 1
        self.reviews.append(review)
        return self.reviews

        # with open('test1.json', mode='w', encoding='utf-8') as f:
        #     json.dump([], f)
		#
        # with open('test1.json', mode='a', encoding='utf-8') as feedsjson:
        #     feeds = []
        #     entry = {'new_key1': 'new_value1'}
        #     feeds.append(entry)
        #     json.dump(feeds, feedsjson)

		# a_dict = {'new_key': 'new_value'}
		#
		# with open('test.json') as f:
    	# 	data = json.load(f)
		#
		# data.update(a_dict)
		#
		# with open('test.json', 'w') as f:
    	# 	json.dump(data, f)

    def get_reviews(self):
        return self.reviews

    def post_new_restaurant(self, restaurant):
        # hard coded input for now, that will also eventually match to client side form
        # same situation as above
		# reviews = []
        # self.reviews.append(review)
        # return reviews
        # test_data = []
        didExist = False
        for r in self.data.values():
            #print(r['name'])
            if restaurant == r['name']:
                didExist = True

        if didExist == False:
            #confirms to work! just don't want to affect the original data set for now
            #self.data.append(restaurant)
            self.newR.append(restaurant)
            return self.newR
            #return False
        else:
            return True

    def get_new_restaurants(self):
        return self.newR
