import re, json, requests
import os
import cherrypy

from _yelp_database import _yelp_database

class StateController:
    def __init__(self, yelp):
        self.yelp = yelp

    # return all state options
    def GET_STATE_OPTIONS(self):
        output = {"result": "success"}

        try:
            output["states"] = self.yelp.get_states()
        except:
            output["states"].append({"result": "error"})
            output["result"] = "error"

        return json.dumps(output)
