# visited_restaurant.py - a class to maintain a structure of user specified
# restaurants to keep track of
from _yelp_database import _yelp_database
from review import review
from collections import defaultdict

import os

class visitedRestaurant:

    def __init__(self, database):
        self.my_restaurants = list()
        self.database = database
        self.data = database.data
        self.locations = database.locations
        self.rev = review(database)

    def match_data(self, restaurant, location):
        # ensure that the restaurant to add is in the database
        for r in self.data.values():
            # look through the data until the restaurant in the correct location is found
            if r["name"] == restaurant and (r["state"] == location or r["city"] == location):
                return True
            else:
                continue
        # return False if the restaurant is not in the database
        return False

    def make_unique(self, restaurant, location, review, visited):
        for r in self.my_restaurants:
            if r['restaurant'] == restaurant and r['location'] == location and r['review'] == review and r['visited'] == visited:
                return True

        return False

    def add_restaurant(self, restaurant, location, review, visited):
        # if the restaurant to add is in the database, add the restaurant to the data structure of visited restaurants
        if not self.match_data(restaurant, location):
            rest = self.rev.post_new_restaurant(restaurant)
        if not self.make_unique(restaurant, location, review, visited):
            info = {"restaurant" : restaurant, "location" : location , "review" : review, "visited" : visited}
            self.my_restaurants.append(info)

    def delete_restaurant(self, restaurant, location):

        self.my_restaurants = [r for r in self.my_restaurants if restaurant != r["restaurant"]]


    def get_favorite_restaurants(self):
        return self.my_restaurants


    def print_visited_restaurants(self):
        print(self.my_restaurants)
