import unittest
import requests
import json

class TestState(unittest.TestCase):

    PORT_NUM = '51039'
    print("Testing Port number: ", PORT_NUM)
    SITE_URL = 'http://student04.cse.nd.edu:' + PORT_NUM
    STATE_URL = SITE_URL + '/state_options/'

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False


    def test_get_state_options(self):
        r = requests.get(self.STATE_URL)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["states"]))
        self.assertEqual(body["states"][0], "NC")
        self.assertEqual(body["states"][-1], "CA")

if __name__ == "__main__":
    unittest.main()
