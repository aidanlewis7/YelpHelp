import unittest
import requests
import json

class TestCuisine(unittest.TestCase):

    PORT_NUM = '51039'
    print("Testing Port number: ", PORT_NUM)
    SITE_URL = 'http://student04.cse.nd.edu:' + PORT_NUM
    CUISINE_URL = SITE_URL + '/cuisine/'
    RESET_URL = SITE_URL + '/reset/'

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False


    def test_get_cuisine_options(self):
        r = requests.get(self.SITE_URL + '/cuisine_options/')
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["cuisines"]))
        self.assertEqual(body["cuisines"][0], "American (Traditional)")
        self.assertEqual(body["cuisines"][-1], "Thai")


    def test_get_cuisine_all_locations(self):
        cuisine = "Mexican"

        data = {'cuisine_type': cuisine}
        r = requests.get(self.CUISINE_URL + '/all_locations/', params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["restaurants"]))
        self.assertTrue(cuisine in body["restaurants"][0]["categories"])
        self.assertTrue(cuisine in body["restaurants"][-1]["categories"])


    def test_get_cuisine(self):
        data = {'cuisine_type': 'Italian', 'location_type': 'state', 'location': 'AZ'}
        r = requests.get(self.CUISINE_URL, params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["restaurants"]))
        self.assertEqual(body["restaurants"][0], "Tanzy")
        self.assertEqual(body["restaurants"][-1], "Italia Cafe & Bistro")


    def test_get_cuisine_percentage(self):
        data = {'cuisine_type': 'Chinese', 'location_type': 'city', 'location': 'Charlotte'}
        r = requests.get(self.CUISINE_URL + "percentage/", params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(body["percentage"])
        self.assertEqual(body["percentage"], 6.274172754619682)


    def test_get_cuisine_all_percentages(self):
        data = {'cuisine_type': 'Mexican', 'location_type': 'state'}
        r = requests.get(self.CUISINE_URL + "all_percentages/", params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["locations"]))
        self.assertEqual(body["locations"]["AZ"], 15.138467560426657)
        self.assertEqual(body["locations"]["NY"], 0)


    def test_get_ratings(self):
        data = {'cuisine_type': 'Japanese', 'location_type': 'state', 'location': 'PA', 'rating': 2.7}
        r = requests.get(self.CUISINE_URL + "ratings/", params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["restaurants"]))
        self.assertEqual(body["restaurants"][0], "Teppanyaki Kyoto")
        self.assertEqual(body["restaurants"][-1], "Benichopsticks Restaurant")


if __name__ == "__main__":
    unittest.main()
