import unittest
import requests
import json

class TestAmbience(unittest.TestCase):

    PORT_NUM = '51039'
    print("Testing Port number: ", PORT_NUM)
    SITE_URL = 'http://student04.cse.nd.edu:' + PORT_NUM

    AMBIENCE_URL = SITE_URL + '/ambience/'
    RESET_URL = SITE_URL + '/reset/'


    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False


    def test_get_ambience_options(self):
        r = requests.get(self.SITE_URL + '/ambience_options/')
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["ambiences"]))
        self.assertEqual(body["ambiences"][0], "casual")
        self.assertEqual(body["ambiences"][-1], "upscale")


    def test_get_ambience_all_locations(self):
        ambience = "hipster"

        data = {'ambience_type': ambience}
        r = requests.get(self.AMBIENCE_URL + "all_locations/", params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["restaurants"]))
        self.assertTrue(ambience in body["restaurants"][0]["attributes"]["Ambience"])
        self.assertTrue(ambience in body["restaurants"][-1]["attributes"]["Ambience"])


    def test_get_ambience(self):
        data = {'ambience_type': 'hipster', 'location_type': 'state', 'location': 'AZ'}
        r = requests.get(self.AMBIENCE_URL, params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["restaurants"]))
        self.assertEqual(body["restaurants"][0], "Spinelli's Pizzeria")
        self.assertEqual(body["restaurants"][-1], "Pat and Waldo's")


    def test_get_ambience_percentage(self):
        data = {'ambience_type': 'romantic', 'location_type': 'city', 'location': 'Charlotte'}
        r = requests.get(self.AMBIENCE_URL + "percentage/", params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(body["percentage"])
        self.assertEqual(body["percentage"], 1.2032660077352815)


    def test_get_ambience_all_percentages(self):
        data = {'ambience_type': 'romantic', 'location_type': 'state'}
        r = requests.get(self.AMBIENCE_URL + "all_percentages/", params=data)
        body = json.loads(r.content)



        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["locations"]))
        self.assertEqual(body["locations"]["WI"], 1.278600269179004)
        self.assertEqual(body["locations"]["NY"], 0)


    def test_get_ratings(self):
        data = {'ambience_type': 'classy', 'location_type': 'state', 'location': 'PA', 'rating': 2.7}
        r = requests.get(self.AMBIENCE_URL + "ratings/", params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["restaurants"]))
        self.assertEqual(body["restaurants"][0], "BoneWorks Grill")
        self.assertEqual(body["restaurants"][-1], "Le Mont")


if __name__ == "__main__":
    unittest.main()
