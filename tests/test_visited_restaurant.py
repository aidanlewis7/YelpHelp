import unittest
import requests
import json

class TestVisitedRestaurant(unittest.TestCase):

    PORT_NUM = '51047'
    print("Testing Port number: ", PORT_NUM)
    SITE_URL = 'http://student04.cse.nd.edu:' + PORT_NUM

    FAVORITES_URL = SITE_URL + '/favorites/'
    DELETE_URL = FAVORITES_URL + "delete/"
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        m = {}
        r = requests.put(self.RESET_URL, data = json.dumps(m))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False


    def test_get_favorites(self):
        self.reset_data()
        r = requests.get(self.FAVORITES_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        test_restaurant = {}
        test_restaurant['location'] = 'TX'
        test_restaurant['review'] = 'Delicious!'
        test_restaurant['visited'] = True

        faves = resp['restaurants']
        for r in faves:
            if r['restaurant'] == "Torchy's":
                test_restaurant = r

        self.assertEqual(test_restaurant['location'], 'TX')
        self.assertEqual(test_restaurant['review'], 'Delicious!')
        self.assertEqual(test_restaurant['visited'], True)


    def test_post_favorites(self):
        self.reset_data()
        f = {}
        f['restaurant'] = "Torchy's"
        f['location'] = "TX"
        f['review'] = 'Delicious!'
        # f['visited'] = True
        r = requests.post(self.FAVORITES_URL, data = json.dumps(f))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        # self.assertEqual(resp['result'], 'success')

        r = requests.get(self.FAVORITES_URL)
        #print(r.content.decode())
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        #print('resp: ', resp)
        for r in resp['restaurants']:
            if r['restaurant'] == "Torchy's":
                self.assertEqual(r['restaurant'], f['restaurant'])
                self.assertEqual(r['location'], f['location'])
                self.assertEqual(r['review'], f['review'])
                # self.assertEqual(r['visited'], f['visited'])

    def test_delete_favorite(self):
        f = {}
        f['restaurant'] = "Torchy's"
        f['location'] = "TX"
        f['review'] = 'Delicious!'
        f['visited'] = True
        r = requests.post(self.FAVORITES_URL, data = json.dumps(f))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        f = {}
        f['restaurant'] = "Chick-fil-a"
        f['location'] = "TX"
        f['review'] = 'Eat More Chicken!'
        f['visited'] = True
        r = requests.post(self.FAVORITES_URL, data = json.dumps(f))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')


        r = requests.get(self.FAVORITES_URL)
        #print(r.content.decode())
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        print("Length before: ", len(resp['restaurants']))
        print(resp)


        m = {}
        m['restaurant'] = "Torchy's Tacos"
        m['location'] = "TX"
        r = requests.post(self.DELETE_URL, data=json.dumps(m))
        print(r.content.decode())
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        print(resp)
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.FAVORITES_URL)
        #print(r.content.decode())
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        print("Length after: ", len(resp['restaurants']))
        print(resp)


        # r = requests.get(self.FAVORITES_URL)
        # self.assertTrue(self.is_json(r.content))
        # resp = json.loads(r.content)
        # self.assertEquals(resp['result'], 'error')


if __name__ == "__main__":
    unittest.main()
