import unittest
import requests
import json

class TestWebService(unittest.TestCase):

    PORT_NUM = '51039'
    SITE_URL = 'http://student04.cse.nd.edu:' + PORT_NUM

    AMBIENCE_URL = SITE_URL + '/ambience/'
    FAVORITES_URL = SITE_URL + '/favorites/'
    RESET_URL = SITE_URL + '/reset/'
    CUISINE_URL = SITE_URL + '/cuisine/'
    QUIZ_URL = SITE_URL + '/quiz/'
    REVIEWS_URL = SITE_URL + '/reviews/'
    STATE_URL = SITE_URL + '/state_options/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        m = {}
        r = requests.put(self.RESET_URL, data = json.dumps(m))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False


    def test_get_ambience_options(self):

        r = requests.get(self.SITE_URL + '/ambience_options/')
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["ambiences"]))
        self.assertEqual(body["ambiences"][0], "casual")
        self.assertEqual(body["ambiences"][-1], "upscale")


    def test_get_ambience_all_locations(self):

        ambience = "hipster"

        data = {'ambience_type': ambience}
        r = requests.get(self.AMBIENCE_URL + "all_locations/", params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["restaurants"]))
        self.assertTrue(ambience in body["restaurants"][0]["attributes"]["Ambience"])
        self.assertTrue(ambience in body["restaurants"][-1]["attributes"]["Ambience"])


    def test_get_ambience(self):


        data = {'ambience_type': 'hipster', 'location_type': 'state', 'location': 'AZ'}
        r = requests.get(self.AMBIENCE_URL, params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["restaurants"]))
        self.assertEqual(body["restaurants"][0], "Spinelli's Pizzeria")
        self.assertEqual(body["restaurants"][-1], "Pat and Waldo's")


    def test_get_ambience_percentage(self):


        data = {'ambience_type': 'romantic', 'location_type': 'city', 'location': 'Charlotte'}
        r = requests.get(self.AMBIENCE_URL + "percentage/", params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(body["percentage"])
        self.assertEqual(body["percentage"], 1.2032660077352815)


    def test_get_ambience_all_percentages(self):


        data = {'ambience_type': 'romantic', 'location_type': 'state'}
        r = requests.get(self.AMBIENCE_URL + "all_percentages/", params=data)
        body = json.loads(r.content)



        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["locations"]))
        self.assertEqual(body["locations"]["WI"], 1.278600269179004)
        self.assertEqual(body["locations"]["NY"], 0)


    def test_get_ratings(self):


        data = {'ambience_type': 'classy', 'location_type': 'state', 'location': 'PA', 'rating': 2.7}
        r = requests.get(self.AMBIENCE_URL + "ratings/", params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["restaurants"]))
        self.assertEqual(body["restaurants"][0], "BoneWorks Grill")
        self.assertEqual(body["restaurants"][-1], "Le Mont")

    def test_get_cuisine_options(self):

        r = requests.get(self.SITE_URL + '/cuisine_options/')
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["cuisines"]))
        self.assertEqual(body["cuisines"][0], "American (Traditional)")
        self.assertEqual(body["cuisines"][-1], "Thai")


    def test_get_cuisine_all_locations(self):

        cuisine = "Mexican"

        data = {'cuisine_type': cuisine}
        r = requests.get(self.CUISINE_URL + '/all_locations/', params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["restaurants"]))
        self.assertTrue(cuisine in body["restaurants"][0]["categories"])
        self.assertTrue(cuisine in body["restaurants"][-1]["categories"])


    def test_get_cuisine(self):

        data = {'cuisine_type': 'Italian', 'location_type': 'state', 'location': 'AZ'}
        r = requests.get(self.CUISINE_URL, params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["restaurants"]))
        self.assertEqual(body["restaurants"][0], "Tanzy")
        self.assertEqual(body["restaurants"][-1], "Italia Cafe & Bistro")


    def test_get_cuisine_percentage(self):

        data = {'cuisine_type': 'Chinese', 'location_type': 'city', 'location': 'Charlotte'}
        r = requests.get(self.CUISINE_URL + "percentage/", params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(body["percentage"])
        self.assertEqual(body["percentage"], 6.274172754619682)


    def test_get_cuisine_all_percentages(self):

        data = {'cuisine_type': 'Mexican', 'location_type': 'state'}
        r = requests.get(self.CUISINE_URL + "all_percentages/", params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["locations"]))
        self.assertEqual(body["locations"]["AZ"], 15.138467560426657)
        self.assertEqual(body["locations"]["NY"], 0)


    def test_get_ratings(self):

        data = {'cuisine_type': 'Japanese', 'location_type': 'state', 'location': 'PA', 'rating': 2.7}
        r = requests.get(self.CUISINE_URL + "ratings/", params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["restaurants"]))
        self.assertEqual(body["restaurants"][0], "Teppanyaki Kyoto")
        self.assertEqual(body["restaurants"][-1], "Benichopsticks Restaurant")

    def test_get_quiz_options(self):

        data = {'state': "NC", "rating": 2, "cuisine": "Italian", "ambience": "casual"}
        r = requests.get(self.QUIZ_URL, params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertEqual(body["state"], data["state"])
        self.assertTrue(body["stars"] >= data["rating"])
        self.assertTrue(body["ambience"][data["ambience"]])
        self.assertTrue(data["cuisine"] in body["categories"])

    def test_get_reviews(self):

        r = requests.get(self.REVIEWS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        test_restaurant = {}
        test_restaurant['restaurant'] = 'Texas Roadhouse'
        test_restaurant['content'] = 'Delicious!'
        test_restaurant['rating'] = 5

        for r in resp['reviews']:
            if r['restaurant'] == "Texas Roadhouse":
                test_review = r
                self.assertEqual(test_review['restaurant'], 'Texas Roadhouse')
                self.assertEqual(test_review['content'], 'Delicious!')
                self.assertEqual(test_review['rating'], 5)

    def test_get_state_options(self):

        r = requests.get(self.STATE_URL)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertTrue(len(body["states"]))
        self.assertEqual(body["states"][0], "NC")
        self.assertEqual(body["states"][-1], "CA")

    def test_get_favorites(self):

        r = requests.get(self.FAVORITES_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        test_restaurant = {}
        test_restaurant['location'] = 'TX'
        test_restaurant['review'] = 'Delicious!'
        test_restaurant['visited'] = True

        faves = resp['restaurants']
        for r in faves:
            if r['restaurant'] == "Torchy's":
                test_restaurant = r

        self.assertEqual(test_restaurant['location'], 'TX')
        self.assertEqual(test_restaurant['review'], 'Delicious!')
        self.assertEqual(test_restaurant['visited'], True)


    def test_post_favorites(self):

        f = {}
        f['restaurant'] = "Torchy's"
        f['location'] = "TX"
        f['review'] = 'Delicious!'

        r = requests.post(self.FAVORITES_URL, data = json.dumps(f))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())


        r = requests.get(self.FAVORITES_URL)

        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())

        for r in resp['restaurants']:
            if r['restaurant'] == "Torchy's":
                self.assertEqual(r['restaurant'], f['restaurant'])
                self.assertEqual(r['location'], f['location'])
                self.assertEqual(r['review'], f['review'])


if __name__ == "__main__":
    unittest.main()

    unittest.reset_data()
