import unittest
import requests
import json

class TestQuiz(unittest.TestCase):

    PORT_NUM = '51039'
    print("Testing Port number: ", PORT_NUM)
    SITE_URL = 'http://student04.cse.nd.edu:' + PORT_NUM
    QUIZ_URL = SITE_URL + '/quiz/'

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False


    def test_get_quiz_options(self):
        data = {'state': "NC", "rating": 2, "cuisine": "Italian", "ambience": "casual"}
        r = requests.get(self.QUIZ_URL, params=data)
        body = json.loads(r.content)

        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        self.assertEqual(body["state"], data["state"])
        self.assertTrue(body["stars"] >= data["rating"])
        self.assertTrue(body["ambience"][data["ambience"]])
        self.assertTrue(data["cuisine"] in body["categories"])


if __name__ == "__main__":
    unittest.main()
