import unittest
import requests
import json

class TestReviews(unittest.TestCase):

    PORT_NUM = '51039'
    print("Testing Port number: ", PORT_NUM)
    SITE_URL = 'http://student04.cse.nd.edu:' + PORT_NUM

    REVIEWS_URL = SITE_URL + '/reviews/'
    RESET_URL = SITE_URL + '/reset/'

    def reset_data(self):
        m = {}
        r = requests.put(self.RESET_URL, data = json.dumps(m))

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_post_reviews(self, restaurant, content, rating):
        self.reset_data()
        f = {}
        f['restaurant'] = "Texas Roadhouse"
        f['content'] = 'Delicious!'
        f['rating'] = 5
        r = requests.post(self.REVIEWS_URL, data = json.dumps(f))
        # print(r.content.decode())
        # self.assertTrue(self.is_json(r.content.decode()))
        # resp = json.loads(r.content.decode())
        # print('resp: ', resp)
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.REVIEWS_URL)
        #print(r.content.decode())
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        #print('resp: ', resp)
        for r in resp['reviews']:
            if r['restaurant'] == "Texas Roadhouse":
                self.assertEqual(r['restaurant'], f['restaurant'])
                self.assertEqual(r['content'], f['content'])
                self.assertEqual(r['rating'], f['rating'])

    def test_get_reviews(self):
        # self.reset_data()
        r = requests.get(self.REVIEWS_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        print(resp)

        test_restaurant = {}
        test_restaurant['restaurant'] = 'Texas Roadhouse'
        test_restaurant['content'] = 'Delicious!'
        test_restaurant['rating'] = 5

        for r in resp['reviews']:
            if r['restaurant'] == "Texas Roadhouse":
                test_review = r
                self.assertEqual(test_review['restaurant'], 'Texas Roadhouse')
                self.assertEqual(test_review['content'], 'Delicious!')
                self.assertEqual(test_review['rating'], 5)

if __name__ == "__main__":
    unittest.main()