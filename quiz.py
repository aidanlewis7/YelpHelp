from _yelp_database import _yelp_database
from cuisine import cuisine
from ambience import ambience
import random


class quiz:

    # inherits from _yelp_database, cuisine, and ambience classes
    def __init__(self, cuisine, ambience):
        self.cuisine = cuisine
        self.ambience = ambience


    def get_quiz_result(self, state, rating, cuisine, ambience):
        cuisine_match = self.cuisine.get_ratings("state", state, cuisine, int(rating))[0]
        ambience_match = self.ambience.get_ratings("state", state, ambience, int(rating))[0]

        cuisine_keys = set()
        ambience_keys = set()

        for r in cuisine_match:
            cuisine_keys.add(r["business_id"])

        for r in ambience_match:
            ambience_keys.add(r["business_id"])

        results = list(cuisine_keys & ambience_keys)

        random.seed()
        result_key = random.sample(results, 1)[0]

        for r in cuisine_match:
            if r["business_id"] == result_key:
                return r
