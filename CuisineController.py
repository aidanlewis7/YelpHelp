import re, json, requests
import os
import cherrypy

from cuisine import cuisine

class CuisineController:
    def __init__(self, c):
        self.c = c

    # return all cuisine options
    def GET_CUISINE_OPTIONS(self):
        output = {"result": "success"}

        try:
            output["cuisines"] = self.c.get_options()
        except:
            output["cuisines"].append({"result": "error"})
            output["result"] = "error"

        return json.dumps(output)


    # return all restaurants for given cuisine (everywhere)
    def GET_CUISINE_ALL_LOCATIONS(self, cuisine_type):
        output = {"restaurants": [], "result": "success"}

        try:
            for r in self.c.get_all_locations(cuisine_type):
                output["restaurants"].append(r)
        except:
            output["restaurants"].append({"result": "error"})
            output["result"] = "error"

        return json.dumps(output)


    # return all restaurants for given cuisine in a region
    def GET_CUISINE(self, cuisine_type, location_type, location):
        output = {"restaurants": [], "result": "success"}

        try:
            for r in self.c.get(location_type, location, cuisine_type):
                output["restaurants"].append(r["name"])
        except:
            output["restaurants"].append({"result": "error"})
            output["result"] = "error"

        return json.dumps(output)

    # return percentage of restaurants with specific cuisine in region
    def GET_CUISINE_PERCENTAGE(self, cuisine_type, location_type, location):
        output = {"percentage": 0, "result": "success"}

        try:
            output["percentage"] = self.c.get_percentage(location_type, location, cuisine_type)
        except:
            output["result"] = "error"

        return json.dumps(output)

    # return percentage of restaurants with specific cuisine in all regions
    def GET_CUISINE_ALL_PERCENTAGES(self, location_type, cuisine_type):
        output = {"locations": {}, "result": "success"}

        try:
            for k, v in self.c.get_all_percentages(location_type, cuisine_type).items():
                output["locations"][k] = v
        except:
            output["result"] = "error"

        return json.dumps(output)

    # return restaurants in region with high enough rating and specified cuisine
    def GET_CUISINE_RATINGS(self, location_type, location, cuisine_type, rating):
        output = {"restaurants": [], "result": "success"}

        try:
            restaurants, trash = self.c.get_ratings(location_type, location, cuisine_type, float(rating))

            for r in restaurants:
                output["restaurants"].append(r["name"])
        except:
            output["restaurants"].append({"result": "error"})
            output["result"] = "error"

        return json.dumps(output)
