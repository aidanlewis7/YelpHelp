from _yelp_database import _yelp_database

class ambience:

    # inherits from _yelp_database class
    def __init__(self, database):
        self.database = database
        self.data = database.data
        self.locations = database.locations

    def get_options(self):

        ambiences = []

        for restaurant in self.data.values():
            if 'attributes' in restaurant and 'Ambience' in restaurant['attributes']:
                for ambience in restaurant['attributes']['Ambience'].keys():
                    ambiences.append(ambience)

                break

        return sorted(ambiences)


    def get_all_locations(self, atmosphere):

        restaurants = []

        # loop through all restaurants
        for restaurant in self.data.values():

            # if restuarant in specific region and has requested ambience, add to list
            if 'attributes' in restaurant and 'Ambience' in restaurant['attributes'] \
            and restaurant['attributes']['Ambience'][atmosphere]:
                restaurants.append(restaurant)

        return restaurants



    def get(self, location_type, location, atmosphere):

        restaurants = []

        # loop through all restaurants
        for restaurant in self.data.values():

            # if restuarant in specific region and has requested ambience, add to list
            if restaurant[location_type] == location and 'attributes' in restaurant \
            and 'Ambience' in restaurant['attributes'] and restaurant['attributes']['Ambience'][atmosphere]:
                restaurants.append(restaurant)

        return restaurants


    def get_percentage(self, location_type, location, atmosphere):
        # get percentage of restaurants in this region with this ambience
        restaurants = self.get(location_type, location, atmosphere)
        return 100 * len(restaurants) / float(self.locations[location_type][location])


    def get_all_percentages(self, location_type, atmosphere):

        # get percentage for every region of specified type
        restaurants = dict()

        for location in self.locations[location_type].keys():
            restaurants[location] = self.get_percentage(location_type, location, atmosphere)

        return restaurants
        #return self.database.sort_dict_by_value(restaurants)

    def get_ratings(self, location_type, location, atmosphere, rating):
        rated_restaurants = []
        ratings = []
        restaurants = self.get(location_type, location, atmosphere)

        # for every restaurant with this ambience in this region
        for restaurant in restaurants:

            # only append restaurant if above specified rating
            if restaurant['stars'] >= rating:
                rated_restaurants.append(restaurant)
                ratings.append(restaurant['stars'])

        #rated_restaurants = self.database.sort_dict_by_value(rated_restaurants)

        #print(rated_restaurants)

        return rated_restaurants, ratings
