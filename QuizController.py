import re, json, requests
import os
import cherrypy

from quiz import quiz

class QuizController:
    def __init__(self, q):
        self.q = q

    # return quiz result
    def GET_QUIZ_RESULT(self, state, rating, cuisine, ambience):
        output = {"result": "success"}

        try:
            result = self.q.get_quiz_result(state, rating, cuisine, ambience)
            info = ["name", "address", "city", "state", "stars"]

            for key in info:
                output[key] = result[key]

            output["categories"] = result["categories"]
            output["ambience"] = result["attributes"]["Ambience"]
        except:
            output["result"] = "error"

        return json.dumps(output)
